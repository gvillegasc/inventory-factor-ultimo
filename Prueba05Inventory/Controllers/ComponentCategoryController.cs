﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;

using Prueba05Inventory.Datos;

namespace Prueba05Inventory.Controllers
{
    public class ComponentCategoryController : Controller
    {
        #region Contexto
        //Atributo contexto
        private inventoryfactorv3Entities _contexto;

        //Propiedad contexto
        public inventoryfactorv3Entities Contexto
        {
            set { _contexto = value; }
            get
            {
                if (_contexto == null)
                    _contexto = new inventoryfactorv3Entities();
                return _contexto;
            }
        }
        #endregion
        // GET: CategoryComponent
        [Authorize]
        public ActionResult Index()
        {
            return View(Contexto.CategoriaComponente.ToList().AsEnumerable());
        }

        [Authorize]
        public ActionResult CrearCategoria()
        {
            return View();
        }

        [HttpPost]
        public ActionResult CrearCategoria(CategoriaComponente newCategory)
        {
            try
            {
                //validamos los datos ingresados
                if (ModelState.IsValid)
                {
                    //registramos la nueva categoria
                    Contexto.CategoriaComponente.Add(newCategory);
                    Contexto.SaveChanges();
                    //llamamos al metodo Index
                    ViewBag.Estado = 1;
                    return View();
                    //muestra la vista "create" con datos ingresados
                }
                return View(newCategory);
            }
            catch
            {
                ViewBag.Estado = 2;
                //Muestra la vista "create" vacia
                return View();
            }
        }

        [Authorize]
        public ActionResult ModificarCategoria(int? id)
        {
            //Si el ID es nulo
            if (id == null)
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);

            //Buscar la categoria a editar
            CategoriaComponente CategoriaEditar = Contexto.CategoriaComponente.Find(id);

            //Si entidad es NULO (categoria no existe)
            if (CategoriaEditar == null)
                return HttpNotFound();

            //envia la categoria a editar a la vista
            return View(CategoriaEditar);
        }
        [HttpPost]
        public ActionResult ModificarCategoria(int? id, CategoriaComponente CategoryEdit)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    //Graba los cambios en la categoria
                    Contexto.Entry(CategoryEdit).State = System.Data.Entity.EntityState.Modified;
                    Contexto.SaveChanges();

                    ViewBag.Estado = 1;
                    return View();
                }
                //Muestra la vista con los datos ingresados
                return View(CategoryEdit);
            }
            catch (Exception ex)
            {
                ViewBag.Estado = 2;
                @ViewBag.Nombre = ex;
                //Muestra la vista vacia
                return View();
            }
        }

        public ActionResult EliminarCategoria(int? id, CategoriaComponente category)
        {
            try
            {
                CategoriaComponente CategoriaEliminar = new CategoriaComponente();
                if (ModelState.IsValid)
                {
                    if (id == null)
                        return new HttpStatusCodeResult(HttpStatusCode.BadRequest);

                    //Buscar la categoria a editar
                    CategoriaEliminar = Contexto.CategoriaComponente.Find(id);

                    //Si entidad es NULO (categoria no existe)
                    if (CategoriaEliminar == null)
                        return HttpNotFound();

                    //Elimina la categoria
                    Contexto.CategoriaComponente.Remove(CategoriaEliminar);
                    Contexto.SaveChanges();
                }
                return Redirect("/ComponentCategory/Index");
            }
            catch
            {
                return Redirect("Index");
            }
        }
    }
}