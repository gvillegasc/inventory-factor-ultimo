﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Data;
using System.Net;
using Prueba05Inventory.Datos;

using Prueba05Inventory.Models;
using System.IO;
using Newtonsoft.Json;

namespace Prueba05Inventory.Controllers
{
    public class UserController : Controller
    {
        #region Contexto
        //Atributo contexto
        private inventoryfactorv3Entities _contexto;

        //Propiedad contexto
        public inventoryfactorv3Entities Contexto
        {
            set { _contexto = value; }
            get
            {
                if (_contexto == null)
                    _contexto = new inventoryfactorv3Entities();
                return _contexto;
            }
        }
        #endregion

        //Funcion para listar el tipo de usuario
        public void ListarTiposUsuarios()
        {
            ViewBag.Tipos = Contexto.TiposUsuarios.Select(x => new SelectListItem
            {
                Text = x.tipo,
                Value = x.tipo_usuario_id.ToString()
            });
        }

        //Obtenemos el ID del API
        [Authorize]
        [HttpPost]
        public dynamic ObtenerDNI(string dni)
        {
            string url = "http://dniruc.apisperu.com/api/v1/dni/"+ dni + "?token=eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJlbWFpbCI6InZpbGxlZ2FzZ2VyYXJkby5qQGdtYWlsLmNvbSJ9.5vd07JTCGJtOvxxEFyaZJQCuZEs8eu5kFm9dpWtJPAE&fbclid=IwAR0kvOS8dy6kADzDV2U0HB-q5rxhIabb4Uu6TBG66SGuwFmPKcK7FsgA_ko";
            HttpWebRequest mywebRequest = (HttpWebRequest)WebRequest.Create(url);

            HttpWebResponse myhttWebResponse = (HttpWebResponse)mywebRequest.GetResponse();
            Stream myStream = myhttWebResponse.GetResponseStream();

            StreamReader myStreamReader = new StreamReader(myStream);

            string Datos = HttpUtility.HtmlDecode(myStreamReader.ReadToEnd());
            dynamic DatosDNI = JsonConvert.DeserializeObject(Datos);

            return DatosDNI;
        }

        //Autorizamos el ROL de administrador
        [Authorize(Roles = "Administrador")]
        public ActionResult ListarUsuario()
        {
            return View(Contexto.Usuarios.ToList().AsEnumerable());
        }

        [Authorize(Roles = "Administrador")]
        public ActionResult CrearUsuario() {
            ListarTiposUsuarios();
            return View();
        }

        [HttpPost]
        public ActionResult CrearUsuario(Usuarios usuarios, Personas personas)
        {
            try
            {
                if (ModelState.IsValid)
                {

                    ListarTiposUsuarios();
                    
                    Contexto.Personas.Add(personas);
                    Contexto.SaveChanges();

                    //Seleccionamos el id de esa persona
                    usuarios.persona_id = personas.persona_id;

                    //Finalmente se inserta usuario
                    Contexto.Usuarios.Add(usuarios);
                    Contexto.SaveChanges();

                    ViewBag.Estado = 1;
                    
                    return View();
                }
                return View();
            }
            catch
            {
                ViewBag.Estado = 2;
                return View();
            }
        }

        [Authorize(Roles = "Administrador")]
        public ActionResult ModificarUsuario(int? id)
        {
            //Si el ID es nulo
            if (id == null)
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);

            ListarTiposUsuarios();
            //Buscar el usuario a editar
            Usuarios UsuarioEditar = Contexto.Usuarios.Find(id);

            Personas PersonaEditar = Contexto.Personas.Find(UsuarioEditar.persona_id);

            InformacionUsuarios usuariosInformacion = new InformacionUsuarios();
            usuariosInformacion.usuarios = UsuarioEditar;
            usuariosInformacion.personas = PersonaEditar;

            //Si entidad es NULO (usuario no existe)
            if (UsuarioEditar == null)
                return HttpNotFound();

            //envia el usuario a editar a la vista
            return View(usuariosInformacion);
        }
        [HttpPost]
        public ActionResult ModificarUsuario(Usuarios usuarios, Personas personas)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    ListarTiposUsuarios();

                    Contexto.Entry(personas).State = System.Data.Entity.EntityState.Modified;
                    Contexto.SaveChanges();

                    //Graba los cambios del usuario
                    Contexto.Entry(usuarios).State = System.Data.Entity.EntityState.Modified;
                    Contexto.SaveChanges();

                    ViewBag.Estado = 1;
                    return View();
                }
                //Muestra la vista Edit con los datos ingresados
                return View();
            }
            catch
            {
                ViewBag.Estado = 2;
                //Muestra la vista vacia
                return View();
            }
        }

        public ActionResult EliminarUsuario(int? id)
        {
            try
            {
                Usuarios UsuariosEliminar = new Usuarios();
                if (ModelState.IsValid)
                {
                    if (id == null)
                        return new HttpStatusCodeResult(HttpStatusCode.BadRequest);

                    //Buscar la categoria a editar
                    UsuariosEliminar = Contexto.Usuarios.Find(id);

                    Personas per = new Personas();
                    per = UsuariosEliminar.Personas;

                    //Eliminamos la persona primero por ser tabla independiente
                    Contexto.Personas.Remove(per);
                    Contexto.SaveChanges();

                    //Eliminamos el usuario 
                    Contexto.Usuarios.Remove(UsuariosEliminar);
                    Contexto.SaveChanges();

                    //Si entidad es NULO (categoria no existe)
                    if (UsuariosEliminar == null)
                        return HttpNotFound();

                    //Elimina la categoria
                    Contexto.Usuarios.Remove(UsuariosEliminar);
                    Contexto.SaveChanges();
                    return Redirect("/User/ListarUsuario");
                }
                return Redirect("/User/ListarUsuario");
            }
            catch
            {
                return Redirect("/User/ListarUsuario");
            }
        }

    }
}