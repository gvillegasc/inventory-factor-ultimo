﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

using System.Web.Security;
using Prueba05Inventory.Datos;

namespace Prueba05Inventory.Controllers
{
    public class LoginController : Controller
    {

        public ActionResult Login()
        {
            return View();
        }

        [HttpPost]
        public ActionResult Login(Usuarios user,string ReturnUrl)
        {
            if (IsValid(user))
            {
                FormsAuthentication.SetAuthCookie(user.username, false);
                return Redirect(ReturnUrl);
            }else
            {
                
                ViewBag.Error = "Usuario o contraseña invalidos";
                return View(user);
            }
        }

        //Se deslogea el usuario
        public ActionResult Logout()
        {
            FormsAuthentication.SignOut();
            return Redirect("/Home/Index");
        }

        //Funcion para validar el usuario, ademas de que se guarda el id en el navegador
        private bool IsValid(Usuarios user)
        {
            try
            {
                inventoryfactorv3Entities Contexto = new inventoryfactorv3Entities();
                var iniciar = (from u in Contexto.Usuarios
                               where u.username == user.username && u.password == user.password.Trim()
                               select u).FirstOrDefault();
                Session["UsuarioID"] = iniciar.usuario_id;
                return (iniciar != null);
            }
            catch
            {
                return (false);
            }
            
        }
    }
}