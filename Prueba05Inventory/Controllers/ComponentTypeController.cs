﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;

using Prueba05Inventory.Datos;

namespace Prueba05Inventory.Controllers
{
    public class ComponentTypeController : Controller
    {
        #region Contexto
        //Atributo contexto
        private inventoryfactorv3Entities _contexto;

        //Propiedad contexto
        public inventoryfactorv3Entities Contexto
        {
            set { _contexto = value; }
            get
            {
                if (_contexto == null)
                    _contexto = new inventoryfactorv3Entities();
                return _contexto;
            }
        }
        #endregion
        // GET: TypeComponent
        [Authorize]
        public ActionResult Index()
        {
            return View(Contexto.TipoComponente.ToList().AsEnumerable());
        }

        [Authorize]
        public ActionResult CrearTipo()
        {
            return View();
        }
        [HttpPost]
        public ActionResult CrearTipo(TipoComponente newTypeComponent)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    Contexto.TipoComponente.Add(newTypeComponent);
                    Contexto.SaveChanges();

                    ViewBag.Estado = 1;
                    return View();
                }
                return View(newTypeComponent);
            }
            catch
            {
                ViewBag.Estado = 2;
                return View();
            }
        }

        [Authorize]
        public ActionResult ModificarTipo(int? id)
        {
            //Si el ID es nulo
            if (id == null)
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);

            //Buscar el tipo a editar
            TipoComponente CategoriaEditar = Contexto.TipoComponente.Find(id);

            //Si entidad es NULO (tipo no existe)
            if (CategoriaEditar == null)
                return HttpNotFound();

            //envia el tipo a editar a la vista

            return View(CategoriaEditar);
        }
        [HttpPost]
        public ActionResult ModificarTipo(TipoComponente TypeUpdate)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    //Graba los cambios del tipo
                    Contexto.Entry(TypeUpdate).State = System.Data.Entity.EntityState.Modified;
                    Contexto.SaveChanges();

                    ViewBag.Estado = 1;
                    return View();
                }
                
                //Muestra la vista con los datos ingresados
                return View(TypeUpdate);
            }
            catch
            {
                ViewBag.Estado = 2;
                //Muestra la vista vacia
                return View();
            }
        }

        public ActionResult EliminarTipo(int? id, TipoComponente type)
        {
            try
            {
                TipoComponente TipoEliminar = new TipoComponente();
                if (ModelState.IsValid)
                {
                    if (id == null)
                        return new HttpStatusCodeResult(HttpStatusCode.BadRequest);

                    //Buscar el tipo a editar
                    TipoEliminar = Contexto.TipoComponente.Find(id);

                    //Si entidad es NULO (tipo no existe)
                    if (TipoEliminar == null)
                        return HttpNotFound();

                    //Elimina el tipo
                    Contexto.TipoComponente.Remove(TipoEliminar);
                    Contexto.SaveChanges();
                }
                return Redirect("/ComponentType/Index");
            }
            catch
            {
                return Redirect("Index");
            }
        }
    }
}