﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;

using Prueba05Inventory.Datos;
using Prueba05Inventory.Models;

namespace Prueba05Inventory.Controllers
{
    public class ComponentController : Controller
    {

        #region Contexto
        //Atributo contexto
        private inventoryfactorv3Entities _contexto;

        //Propiedad contexto
        public inventoryfactorv3Entities Contexto
        {
            set { _contexto = value; }
            get
            {
                if (_contexto == null)
                    _contexto = new inventoryfactorv3Entities();
                return _contexto;
            }
        }
        #endregion

        public ActionResult ListarComponente()
        {
            return View(Contexto.Componentes.ToList().AsEnumerable());
        }

        [Authorize]
        public ActionResult CrearComponente()
        {
            ListarCaracteristicasComponente();
            return View();
        }

        [HttpPost]
        public ActionResult CrearComponente(HttpPostedFileBase imagenComponente, Componentes componentes)
        {
            SubirImagen(imagenComponente, componentes);

            try
            {
                //validamos los datos ingresados
                if (ModelState.IsValid)
                {
                    //registramos el nuevo componente
                    Contexto.Componentes.Add(componentes);
                    Contexto.SaveChanges();

                    //llamamos al metodo Index
                    ViewBag.Estado = 1;
                    return View();
                    //muestra la vista con datos ingresados
                }
                return View();
            }
            catch
            {
                ViewBag.Estado = 2;
                //Muestra la vista vacia
                return View();
            }
        }

        [Authorize]
        public ActionResult ModificarComponente(int? id)
        {
            //Si el ID es nulo
            if (id == null)
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);

            //Buscar el componente a editar
            ListarCaracteristicasComponente();
            Componentes ComponenteEditar = Contexto.Componentes.Find(id);

            //Si entidad es NULO (componente no existe)
            if (ComponenteEditar == null)
                return HttpNotFound();

            InformacionComponentes info = new InformacionComponentes();
            info.componentes = ComponenteEditar;


            //envia el componente a editar a la vista
            return View(info);
        }

        [HttpPost]
        public ActionResult ModificarComponente(HttpPostedFileBase imagenComponente, Componentes componentes)
        {
            //return View();

            SubirImagen(imagenComponente, componentes);
            try
            {
                if (ModelState.IsValid)
                {
                    /**/

                    //Graba los cambios del componente
                    Contexto.Entry(componentes).State = System.Data.Entity.EntityState.Modified;
                    Contexto.SaveChanges();

                    ViewBag.Estado = 1;
                    return View();
                }
                
                //Muestra la vista con los datos ingresados
                return View(componentes);
            }
            catch
            {
                ViewBag.Estado = 2;
                //Muestra la vista vacia
                return View();
            }
        }

        public ActionResult EliminarComponente(int? id)
        {
            try
            {
                Componentes ComponenteEliminar = new Componentes();
                if (ModelState.IsValid)
                {
                    if (id == null)
                        return new HttpStatusCodeResult(HttpStatusCode.BadRequest);

                    //Buscar el componente a editar
                    ComponenteEliminar = Contexto.Componentes.Find(id);

                    //Si entidad es NULO (componente no existe)
                    if (ComponenteEliminar == null)
                        return HttpNotFound();

                    //Elimina el componente
                    Contexto.Componentes.Remove(ComponenteEliminar);
                    Contexto.SaveChanges();
                    return Redirect("/Component/ListarComponente");
                }
                return View(ComponenteEliminar);
            }
            catch
            {
                return View();
            }
        }

        //Funcion para listar las caracteristicas del componente
        public void ListarCaracteristicasComponente()
        {
            ViewBag.TipoComponente = Contexto.TipoComponente.Select(x => new SelectListItem
            {
                Text = x.tipo,
                Value = x.tipo_componente_id.ToString()
            });

            ViewBag.CategoriaComponente = Contexto.CategoriaComponente.Select(x => new SelectListItem
            {
                Text = x.categoria,
                Value = x.categoria_componente_id.ToString()
            });
        }

        //Funcion para convertir la imagen subida
        public ActionResult ConvertirImagen(int? id)
        {
            var imagenComponente = Contexto.Componentes.Where(x => x.componente_id == id).FirstOrDefault();
            return File(imagenComponente.imagen, "image/jpeg");
        }

        //Funcion para subir la imagen a la base de datos
        public void SubirImagen(HttpPostedFileBase imagenComponente,  Componentes componentes)
        {
            ListarCaracteristicasComponente();
            if (imagenComponente != null && imagenComponente.ContentLength > 0)
            {
                byte[] imagenData = null;
                using (var binaryComponente = new BinaryReader(imagenComponente.InputStream))
                {
                    imagenData = binaryComponente.ReadBytes(imagenComponente.ContentLength);
                }
                componentes.imagen = imagenData;
            }
            else
            {
                var comp = (from co in Contexto.Componentes
                            where co.componente_id == componentes.componente_id
                            select co.imagen).FirstOrDefault();

                componentes.imagen = comp;
            }
        }

    }
}