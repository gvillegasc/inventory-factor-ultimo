﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

using System.Web.Helpers;
using System.Collections;

using Prueba05Inventory.Datos;

namespace Prueba05Inventory.Controllers
{
    public class HomeController : Controller
    {

        [Authorize]
        public ActionResult Index()
        {
            
            inventoryfactorv3Entities Contexto = new inventoryfactorv3Entities();
            return View(Contexto.Movimientos.ToList().AsEnumerable());
        }

        [Authorize]
        public ActionResult Nosotros()
        {
            return View();
        }

        //Creamos el grafico de la cantidad de movimientos que se hizo
        public ActionResult GraficoComponentes()
        {
            inventoryfactorv3Entities Contexto = new inventoryfactorv3Entities();

            ArrayList x = new ArrayList();
            ArrayList y = new ArrayList();

            var query = (from c in Contexto.CabecerasComponentes
                         where c.Movimientos.TiposMovimientos.tipo == "Salida"
                         select new {
                             componentes = c.Componentes.nombre
                         });
            query.ToList().ForEach(r => x.Add(r.componentes));
            query.ToList().ForEach(r => y.Add(query.Count()));

            new Chart(width: 600, height: 400, theme: ChartTheme.Green)
                .AddTitle("Columnas")
                .AddSeries("Default", chartType: "Column", xValue: x, yValues: y)
                .Write("bmp");
            return null;
        }
    }
}