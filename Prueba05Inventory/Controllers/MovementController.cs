﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

using Microsoft.Reporting.WebForms;
using Prueba05Inventory.Datos;
using Prueba05Inventory.Models;

namespace Prueba05Inventory.Controllers
{
    public class MovementController : Controller
    {

        #region Contexto
        //Atributo contexto
        private inventoryfactorv3Entities _contexto;

        //Propiedad contexto
        public inventoryfactorv3Entities Contexto
        {
            set { _contexto = value; }
            get
            {
                if (_contexto == null)
                    _contexto = new inventoryfactorv3Entities();
                return _contexto;
            }
        }
        #endregion

        public ActionResult ListarMovimiento()
        {
            //Listar los datos de la bd
            var listado = (from c in Contexto.CabecerasComponentes
                          select new
                          {
                              c.Movimientos.fecha,
                              c.cantidad,
                              c.Movimientos.precio_total,
                              c.Componentes.nombre,
                              c.Movimientos.Usuarios.username,
                              c.Movimientos.TiposMovimientos.tipo
                          }).ToList();

            var rptviewer = new ReportViewer();
            rptviewer.ProcessingMode = ProcessingMode.Local;
            rptviewer.LocalReport.ReportPath = Request.MapPath(Request.ApplicationPath) + @"Reportes/ReporteMovimiento.rdlc";

            //Agregar el listado de CabeceraComponentes
            ReportDataSource rptdatasource = new ReportDataSource("dsCabeceraComponente", listado);
            rptviewer.LocalReport.DataSources.Add(rptdatasource);

            rptviewer.SizeToReportContent = true;

            ViewBag.ReportViewer = rptviewer;
            return View();
        }

        [Authorize]
        public ActionResult Componente()
        {
            ListarComplementos();
            return View();
        }

        [HttpPost]
        public ActionResult Componente(CabecerasComponentes cabecerasComponentes, int componente_id, int tipo_movimiento_id)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    ListarComplementos();
                    

                    Movimientos m = new Movimientos();

                    //Seteamos el componente
                    var comp = (from c in Contexto.Componentes
                                where c.componente_id == componente_id
                                select c).FirstOrDefault();

                    Componentes com = new Componentes();
                    com = comp;

                    //Obtenemos la fecha actual
                    DateTime hoy = DateTime.Today;

                    //Seteamos los datos
                    m.fecha = hoy;
                    m.precio_total = comp.precio * cabecerasComponentes.cantidad;
                    m.cantidad_total = cabecerasComponentes.cantidad;
                    m.usuario_id = int.Parse(Session["UsuarioID"].ToString());
                    m.tipo_movimiento_id = tipo_movimiento_id;

                    Contexto.Movimientos.Add(m);
                    Contexto.SaveChanges();

                    //Podemos el id de cada uno mas el subtotal
                    cabecerasComponentes.movimiento_id = m.movimiento_id;
                    cabecerasComponentes.componente_id = componente_id;
                    cabecerasComponentes.sub_total = comp.precio * cabecerasComponentes.cantidad;

                    //Si el tipo de movimiento es entrada entonces aumenta al stock
                    if (tipo_movimiento_id == 33)
                    {
                        com.stock = comp.stock + cabecerasComponentes.cantidad;
                    }
                    //Si es salida, disminuye del stock
                    else
                    {
                        Componentes c = new Componentes();
                        com.stock = comp.stock - cabecerasComponentes.cantidad;
                    }

                    //Guardamos los cambios del componente
                    Contexto.Entry(com).State = System.Data.Entity.EntityState.Modified;
                    Contexto.SaveChanges();

                    //Guardamos los cambios de CabeceraComponente
                    Contexto.CabecerasComponentes.Add(cabecerasComponentes);
                    Contexto.SaveChanges();

                    ViewBag.Estado = 1;
                    return View();
                }
                return View(cabecerasComponentes);
            }
            catch
            {
                ViewBag.Estado = 2;

                return View();
            }
        }
        //Funcion para listar los complementos del movimiento de la BD
        public void ListarComplementos()
        {
            ViewBag.Componente = Contexto.Componentes.Select(x => new SelectListItem
            {
                Text = x.nombre,
                Value = x.componente_id.ToString()
            });

            ViewBag.TipoMovimiento = Contexto.TiposMovimientos.Select(x => new SelectListItem
            {
                Text = x.tipo,
                Value = x.tipo_movimiento_id.ToString()
            });
        }
    }
}
 