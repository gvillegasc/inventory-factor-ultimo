﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

using Prueba05Inventory.Datos;

namespace Prueba05Inventory.Models
{
    //Modelo para mostrar los los movimientos de los componentes
    public class MovimientoComponentes
    {
        public MovimientoComponentes movimientoComponentes;
        public CabecerasComponentes cabecerasComponentes;

        public int componente_id { get; set; }
        public int tipo_movimiento_id { get; set; }
    }
}