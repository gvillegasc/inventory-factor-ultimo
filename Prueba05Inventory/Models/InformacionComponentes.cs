﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

using Prueba05Inventory.Datos;

namespace Prueba05Inventory.Models
{
    //Modelo para mostrar la Informacion del Componente
    public class InformacionComponentes
    {
        public Componentes componentes { get; set; }
        public TipoComponente tipoComponente { get; set; }
        public CategoriaComponente categoriaComponente { get; set; }
    }
}