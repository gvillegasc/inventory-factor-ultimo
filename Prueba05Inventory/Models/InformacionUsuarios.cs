﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

using Prueba05Inventory.Datos;

namespace Prueba05Inventory.Models
{
    //Modelo para mostrar la Informacion de los Usuarios
    public class InformacionUsuarios
    {
        public Usuarios usuarios { get; set; }
        public Personas personas { get; set; }
        public TiposUsuarios tipos_usuarios { get; set; }
    }
}